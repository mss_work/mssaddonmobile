﻿using AddonSeidorMobile.commons;
using AddonSeidorMobile.conexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddonSeidorMobile.dao
{
    public class OpcionesSocioDAO: FormCommon
    {
        public const string QUERY_LIST_OPCIONES = "SELECT  \"Code\" as \"Código\", \"Name\" as \"Descripción\", " +
            " \"U_MSSM_OBL\" as \"Obl. Lead\", \"U_MSSM_OBF\" as \"Obl. Final\", \"U_MSSM_OBC\" as \"Obl. Competencia\" " +
            "  from \"@MSSM_SNF\" ";

        public static int obtenerUltimoId()
        {
            int res = 1;
            SAPbobsCOM.Recordset oRS = null;

            try
            {
                oRS = Conexion.company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery("select IFNULL(max(\"DocEntry\"),0) + 1  as \"Result\" from \"@MSSM_SNF\"");

                if (oRS.RecordCount > 0)
                {
                    res = int.Parse(oRS.Fields.Item("Result").Value.ToString().Trim());
                }

            }
            catch (Exception)
            {
                res = 1;
            }
            finally
            {
                if (oRS != null)
                    LiberarObjetoGenerico(oRS);
            }


            return res;
        }

    }
}
