﻿using AddonSeidorMobile.commons;
using AddonSeidorMobile.conexion;
using AddonSeidorMobile.data_schema.tablas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AddonSeidorMobile.dao
{
    public class ConfiguracionSNDAO:FormCommon
    {
        public static bool registrar(string codigo, string descripcion, string vLead, string vFinal, string vCliente)
        {
            var res = true;

            SAPbobsCOM.GeneralService mService = null;
            SAPbobsCOM.GeneralData mTipoUsuario = null;
            SAPbobsCOM.Recordset oRS = null;

            try
            {
                oRS = Conexion.company.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRS.DoQuery("SELECT COUNT(*) as \"Counter\" FROM \"@MSSM_SNF\" WHERE \"Code\" = '" + codigo + "'");

                if (int.Parse(oRS.Fields.Item("Counter").Value.ToString()) == 0)
                {
                    mService = Conexion.company.GetCompanyService().GetGeneralService(ConfiguracionSN.getTabla().nombre);
                    mTipoUsuario = mService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                    mTipoUsuario.SetProperty("Code", codigo);
                    mTipoUsuario.SetProperty("Name", descripcion);
                    mTipoUsuario.SetProperty("U_MSSM_OBL", vLead);
                    mTipoUsuario.SetProperty("U_MSSM_OBF", vFinal);
                    mTipoUsuario.SetProperty("U_MSSM_OBC", vCliente);
                    mService.Add(mTipoUsuario);
                }
            }
            catch (Exception e)
            {
                res = false;
                StatusMessageError("ConfiguracionSNDAO > registrar() > " + e.Message);
            }
            finally
            {
                if (mService != null)
                    LiberarObjetoGenerico(mService);

                if (mTipoUsuario != null)
                    LiberarObjetoGenerico(mTipoUsuario);

                if (oRS != null)
                    LiberarObjetoGenerico(oRS);
            }

            return res;
        }

        public static bool actualizar(string codigo, string descripcion, string vLead, string vFinal, string vCliente)
        {
            var res = true;

            SAPbobsCOM.GeneralService mService = null;
            SAPbobsCOM.GeneralDataParams searchParams = null;
            SAPbobsCOM.GeneralData mTipoUsuario = null;

            try
            {

                mService = Conexion.company.GetCompanyService().GetGeneralService(ConfiguracionSN.getTabla().nombre);
                mTipoUsuario = mService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                searchParams = mService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);
                searchParams.SetProperty("Code", codigo);

                mTipoUsuario = mService.GetByParams(searchParams);
                mTipoUsuario.SetProperty("Name", descripcion);
                mTipoUsuario.SetProperty("U_MSSM_OBL", vLead);
                mTipoUsuario.SetProperty("U_MSSM_OBF", vFinal);
                mTipoUsuario.SetProperty("U_MSSM_OBC", vCliente);

                mService.Update(mTipoUsuario);
            }
            catch (Exception e)
            {
                res = false;
                StatusMessageError("ConfiguracionSNDAO > actualizar() > " + e.Message);
            }
            finally
            {
                if (mService != null)
                    LiberarObjetoGenerico(mService);

                if (searchParams != null)
                    LiberarObjetoGenerico(searchParams);

                if (mTipoUsuario != null)
                    LiberarObjetoGenerico(mTipoUsuario);
            }

            return res;
        }

    }
}
