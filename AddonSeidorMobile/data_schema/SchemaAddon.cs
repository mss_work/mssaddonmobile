﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AddonSeidorMobile.dao;
using AddonSeidorMobile.data_schema.tablas;

namespace AddonSeidorMobile.data_schema
{
    public class SchemaAddon
    {
        public static List<TablaBean> tablasADDON()
        {
            var tables = new List<TablaBean>();

            tables.Add(Movil.getTabla());
            tables.Add(TipoUsuario.getTabla());
            tables.Add(Vendedor.getTabla());
            tables.Add(Vendedor.getTablaDet1());
            tables.Add(Vehiculo.getTabla());
            tables.Add(ConfiguracionSN.getTabla());
            //tables.Add(Vendedor.getTablaDet3());

            return tables;
        }

        public static List<CampoBean> camposADDON()
        {
            var campos = new List<CampoBean>();

          /*  campos.AddRange(Movil.getCamposTabla());
            campos.AddRange(TipoUsuario.getCamposTabla());
            campos.AddRange(Vendedor.getCamposCabe());
            campos.AddRange(Vendedor.getCamposDet1());
            //campos.AddRange(Vendedor.getCamposDet2());
            //campos.AddRange(Vendedor.getCamposDet3());
            campos.AddRange(OrdenVenta.getCamposTabla());
            campos.AddRange(PagosRecibidos.getCamposTabla());
            campos.AddRange(SocioNegocio.getCamposTabla());
            campos.AddRange(SocioNegocio.getCamposTablaDirecciones());
            campos.AddRange(ListaPrecio.getCamposTabla());
            campos.AddRange(ConfiguracionSN.getCamposTabla());
            campos.AddRange(Almacen.getCamposTabla());
            campos.AddRange(Articulo.getCamposTabla());*/
            campos.AddRange(Actividad.getCamposTabla());
            return campos;
        }

        public static List<ObjetoBean> objetosADDON()
        {
            var objects = new List<ObjetoBean>();

            objects.Add(Movil.getObjeto());
            objects.Add(TipoUsuario.getObjeto());
            objects.Add(Vendedor.getObjeto());
            objects.Add(Vehiculo.getObjeto());
            objects.Add(ConfiguracionSN.getObjeto());

            //Insert objects
            Actividad.addActivityTypes("Sin pedido de venta");
            Actividad.addActivityTypes("Entrega no posible");
            Actividad.addActivityTypes("Compromiso de pago");

            ConfiguracionSNDAO.registrar("1", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("2", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("3", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("4", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("5", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("6", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("7", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("8", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("9", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("10", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("11", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("12", "", "N", "N", "N");
            ConfiguracionSNDAO.registrar("13", "", "N", "N", "N");
            ConfiguracionSNDAO.registrar("14", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("15", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("16", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("17", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("18", "", "N", "N", "N");
            ConfiguracionSNDAO.registrar("19", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("20", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("21", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("22", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("23", "", "Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("24", "", "N", "Y", "N");
            ConfiguracionSNDAO.registrar("25", "", "N", "N", "N");
            ConfiguracionSNDAO.registrar("26", "","Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("27", "","Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("28", "","Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("29", "","Y", "Y", "Y");
            ConfiguracionSNDAO.registrar("30", "", "Y", "Y", "Y");

            return objects;
        }
    }
}
